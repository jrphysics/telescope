import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt


def load_spectrum(fp):
    data = np.loadtxt(fp, delimiter=',')
    freq = data[:, 0]
    signal = data[:, 1]
    return freq, signal


def remove_spikes(signal):
    signal_diff = np.diff(np.abs(signal), prepend=0)
    std_dev = np.std(signal_diff)
    sigma = std_dev * 3

    signal_filtered = np.copy(signal)

    # Will filter out these points
    idx_outliers = np.where(np.abs(signal) > np.mean(signal) + sigma)[0]

    nb = 3  # Neighbours to consider
    for idx in idx_outliers:
        signal_filtered[idx] = (np.sum(signal[idx-nb:idx+nb+1]) - signal[idx])/(2*nb)

    # Show filtering effect
    # plt.figure(figsize=(10, 5))
    # plt.subplot(1, 2, 1)
    # plt.plot(signal_diff)
    # plt.axhline(sigma, linestyle='--', color='black')
    # # plt.axhline(abs_mean, linestyle='--', color='red')
    # plt.axhline(-sigma, linestyle='--', color='black')
    # # plt.axhline(-abs_mean, linestyle='--', color='red')
    # # plt.ylim([-sigma3 * 2, sigma3 * 2])
    #
    # plt.subplot(1, 2, 2)
    # plt.plot(signal, '.')
    # plt.plot(signal_filtered, '--')
    #
    # plt.show()

    return signal_filtered


data_folder = Path('/home/julio/pCloudDrive/TELescope_data/20220621_data')
# data_folder = Path('/Users/julio.rodriguez/Documents/raddiotelescope data 2220621')
data_folder = Path('/Users/julio.rodriguez/Documents/telescope/radiotelescope data 5-12_07_22')

# for fp in data_folder.glob('*ein_0_0*.csv'):
#     freq_dark, signal_dark = load_spectrum(fp)
#     signal_dark = remove_spikes(signal_dark)

for fp in sorted(data_folder.glob('*.csv'), key=lambda x: x.stem.split('_')[0]):
    freq, signal = load_spectrum(fp)
    print(fp.stem)

    print('d_freq = {:.2f} KHz'.format(np.mean(np.diff(freq)) * 1E3))

    # if len(freq) == len(freq_dark) and \
    #         (freq.min() == freq_dark.min() and freq.max() == freq_dark.max()):

    signal_filt = remove_spikes(signal)
    signal_corr = signal_filt# - signal_dark

    plt.figure(figsize=(10, 5))
    plt.suptitle(fp.stem)
    plt.subplot(1, 2, 1)
    plt.title('Raw')
    plt.plot(freq, signal)
    plt.subplot(1, 2, 2)
    plt.title('Filtered')
    plt.plot(freq, signal_filt)

    plt.show()
