import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt


def load_spectrum(fp):
    data = np.loadtxt(fp, delimiter=',')
    data_freq = data[:, 0]
    data_signal = data[:, 1]
    return data_freq, data_signal


def alt_az(fp):
    name_list = fp.stem.split('_')
    az = name_list[-3]
    alt = name_list[-2]
    return alt, az


if __name__ == '__main__':
    # data_folder = Path('/Users/julio.rodriguez/Documents/telescope/radiotelescope data 2220621')
    data_folder = Path('/Users/julio.rodriguez/Documents/telescope/radiotelescope data 5-12_07_22')

    hot = data_folder / '2022-07-12_20.54.12.3_soil_t300s_0_-90_spectrum.csv'
    # hot = data_folder / '2022-07-12_20.08.04.3_hot_t300s_0_-20_spectrum.csv'
    # hot = data_folder / '2022-07-12_20.14.41.2_hot_t300s_0_-90_spectrum.csv'
    cold = data_folder / '2022-07-12_20.21.06.4_cold_t300s_0_90_spectrum.csv'

    meas_list = [x for x in data_folder.glob('2022-07-12*sky*.csv')]

    freq_hot, signal_hot = load_spectrum(hot)
    freq_cold, signal_cold = load_spectrum(cold)

    t_hot = 300
    t_cold = 10

    G = (signal_hot - signal_cold) / (t_hot - t_cold)
    t_system = (t_hot - t_cold * (signal_hot / signal_cold)) / ((signal_hot / signal_cold) - 1)

    # Plot calibration data
    fig, axs = plt.subplots(1, 2, figsize=(10, 5))
    axs[0].set_title('Cold & hot signals')
    axs[0].set_xlabel('Frequency (MHz)')
    axs[0].set_ylabel('Intensity (a.u.)')
    axs[0].plot(freq_cold, signal_cold, label='cold')
    axs[0].plot(freq_hot, signal_hot, label='hot')
    axs[0].legend()
    axs[0].grid()

    axs[1].set_title('System temp. and gain')
    axs[1].set_xlabel('Frequency (MHz)')
    axs[1].set_ylabel('Temperature (K)')
    axs[1].plot(freq_hot, t_system, label='System temperature')
    ax2 = axs[1].twinx()
    ax2.set_ylabel('Gain')
    ax2.plot(freq_hot, G, c='orange', label='Gain')
    axs[1].legend(loc=2)
    ax2.legend(loc=1)
    axs[1].grid()

    fig.tight_layout()

    # Plot measurements
    fig, axs = plt.subplots(1, 2, figsize=(10, 5))
    fig.suptitle('MEAS @ 20 alt')
    for meas in meas_list:
        freq, signal = load_spectrum(meas)
        t_object = signal / G - t_system
        azimuth = alt_az(meas)[1]

        axs[0].plot(freq, signal, label=azimuth)
        # axs[1].plot(freq, (signal - signal_cold) / (signal_hot - signal_cold), label='{}'.format(alt_az(fp)[0]))
        axs[1].plot(freq, t_object, label=azimuth)

        axs[0].axvline(1420.4, ls=':', c='k')
        axs[1].axvline(1420.4, ls=':', c='k')

    axs[0].set_title('Raw signal')
    axs[0].set_xlabel('Frequency (MHz)')
    axs[0].set_ylabel('Intensity (a.u.)')
    axs[0].legend(title='Azimuth')
    axs[0].grid()

    axs[1].set_title('Calibrated signal')
    axs[1].set_xlabel('Frequency (MHz)')
    axs[1].set_ylabel('Temperature (K)')
    axs[1].legend(title='Azimuth')
    axs[1].grid()

    fig.tight_layout()

    plt.show()
